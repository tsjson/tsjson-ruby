class TestUtils
  def self.dedent(str)
    trimmedStr = str.sub(/\A\n*/m, "\\1").sub(/[ \t]*\z/, '')

    # fixes indentation by removing leading spaces and tabs from each line
    indent = ''
    trimmedStr.split('').each do |char|
      break if (char != ' ' && char != "\t")
      indent += char
    end
    return trimmedStr.gsub(Regexp.new('^' + indent, 'mg'), '') # remove indent
  end
end
