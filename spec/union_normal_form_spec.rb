RSpec.describe 'union normal form' do
  typeA = ObjectType.new({ f: { type: TSJSONString } })
  typeB = ObjectType.new({ f: { type: TSJSONString } })
  typeC = ObjectType.new({ f: { type: TSJSONString } })
  typeD = ObjectType.new({ f: { type: TSJSONString } })

  typeE = Intersection.new([typeA, typeB])
  typeF = Intersection.new([typeC, typeD])
  typeG = Intersection.new([typeE, typeC])
  typeH = Intersection.new([typeE, typeF])
  typeI = Union.new([typeA, typeB])
  typeJ = Union.new([typeC, typeD])
  typeK = Intersection.new([typeI, typeF])
  typeL = Intersection.new([typeI, typeJ])

  typeM = ObjectType.new({ f: { type: TSJSONString } })
  typeN = ObjectType.new({ f: { type: TSJSONString } })

  it '(A & B) & C => A & B & C' do
    expect(typeG.normal_form).to be_a_kind_of(Intersection)
    expect(typeG.normal_form.types).to eq([typeA, typeB, typeC])
  end

  it '(A | B) & (C & D) => (A & C & D) | (B & C & D)' do
    expect(typeK.normal_form).to be_a_kind_of(Union)

    expect(typeK.normal_form.types[0]).to be_a_kind_of(Intersection)
    expect(typeK.normal_form.types[0].types).to eq([typeA, typeC, typeD])

    expect(typeK.normal_form.types[1]).to be_a_kind_of(Intersection)
    expect(typeK.normal_form.types[1].types).to eq([typeB, typeC, typeD])
  end

  it '(A | B) & (C | D) => (A & C) | (A & D) | (B & C) | (B & D)' do
    expect(typeL.normal_form).to be_a_kind_of(Union)

    expect(typeL.normal_form.types[0]).to be_a_kind_of(Intersection)
    expect(typeL.normal_form.types[0].types).to eq([typeA, typeC])

    expect(typeL.normal_form.types[1]).to be_a_kind_of(Intersection)
    expect(typeL.normal_form.types[1].types).to eq([typeA, typeD])

    expect(typeL.normal_form.types[2]).to be_a_kind_of(Intersection)
    expect(typeL.normal_form.types[2].types).to eq([typeB, typeC])

    expect(typeL.normal_form.types[3]).to be_a_kind_of(Intersection)
    expect(typeL.normal_form.types[3].types).to eq([typeB, typeD])
  end

  it 'A & (B & (C | D)) => (A & B & C) | (A & B & D)' do
    type =
      Intersection.new(
        [typeA, Intersection.new([typeB, Union.new([typeC, typeD])])]
      )

    expect(type.normal_form).to be_a_kind_of(Union)

    expect(type.normal_form.types[0]).to be_a_kind_of(Intersection)
    expect(type.normal_form.types[0].types).to eq([typeA, typeB, typeC])

    expect(type.normal_form.types[1]).to be_a_kind_of(Intersection)
    expect(type.normal_form.types[1].types).to eq([typeA, typeB, typeD])
  end

  it 'A | (B & (C | D) & (M | N)) => A | (B & C & M) | (B & C & N) | (B & D & M) | (B & D & N)' do
    type =
      Union.new(
        [
          typeA,
          Intersection.new(
            [typeB, Union.new([typeC, typeD]), Union.new([typeM, typeN])]
          )
        ]
      )

    expect(type.normal_form).to be_a_kind_of(Union)

    expect(type.normal_form.types[0]).to eq(typeA)

    expect(type.normal_form.types[1]).to be_a_kind_of(Intersection)
    expect(type.normal_form.types[1].types).to eq([typeB, typeC, typeM])

    expect(type.normal_form.types[2]).to be_a_kind_of(Intersection)
    expect(type.normal_form.types[2].types).to eq([typeB, typeC, typeN])

    expect(type.normal_form.types[3]).to be_a_kind_of(Intersection)
    expect(type.normal_form.types[3].types).to eq([typeB, typeD, typeM])

    expect(type.normal_form.types[4]).to be_a_kind_of(Intersection)
    expect(type.normal_form.types[4].types).to eq([typeB, typeD, typeN])
  end
end
