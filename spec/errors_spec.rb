require './lib/errors/index.rb'
include TSJSON

RSpec.describe 'errors' do
  it 'LiteralValidationError' do
    expect(
      LiteralValidationError.new(expected_value: 'A', received_value: 'B')
        .to_human_json
    ).to eq({ message: 'Expected value `A`. Received value `B`' })
  end

  it 'ObjectValidationError' do
    expect(
      ObjectValidationError.new(
        errors: [{ field: 'field_name', error: UnexpectedFieldError.new }]
      ).to_human_json
    ).to eq(
      {
        details: [{ message: 'Unexpected fields: field_name' }],
        message: 'Invalid object'
      }
    )
  end

  it 'ListValidationError' do
    expect(
      ListValidationError.new(
        errors: [
          {
            index: 0,
            error:
              LiteralValidationError.new(
                expected_value: 'A', received_value: 'B'
              )
          }
        ]
      ).to_human_json
    ).to eq(
      {
        message: 'One or more items are invalid',
        details: [{ message: '0: Expected value `A`. Received value `B`' }]
      }
    )
  end

  it 'ScalarUnionValidationError' do
    expect(
      ScalarUnionValidationError.new(
        expected_types: %w[A B], received_type: 'C', received_value: 'c'
      ).to_human_json
    ).to eq(
      { message: 'Expected types: `A, B`. Received value `c` of type `C`' }
    )
  end

  it 'ScalarValidationError' do
    expect(
      ScalarValidationError.new(
        expected_type: 'A', received_type: 'C', received_value: 'c'
      ).to_human_json
    ).to eq({ message: 'Expected type `A`. Received value `c` of type `C`' })
  end
end
