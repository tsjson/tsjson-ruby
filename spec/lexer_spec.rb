require './lib/language/lexer/lexer.rb'
require './lib/language/source.rb'
require_relative './utils.rb'
include TSJSON

def lex_one(str)
  lexer = Lexer.new(Source.new(str))
  lexer.advance.toJSON
end

def lex_second(str)
  lexer = Lexer.new(Source.new(str))
  lexer.advance
  lexer.advance.toJSON
end

def expect_syntax_error(text)
  lex_second(text)
  expect(nil)
rescue => e
  expect(e.toJSON)
end

def expect_syntax_error_string(text)
  lex_second(text)
  expect(nil)
rescue => e
  expect(e.to_s + "\n")
end

RSpec.describe 'lexer' do
  it 'disallows uncommon control characters' do
    expect_syntax_error("\u0007").to match(
      {
        message:
          'Syntax Error: Cannot contain the invalid character "\\u0007".',
        locations: [{ line: 1, column: 1 }]
      }
    )
  end

  it 'accepts BOM header' do
    expect(lex_one("\uFEFF foo")).to include(
      { kind: TokenKind::NAME, start: 2, end: 5, value: 'foo' }
    )
  end

  it 'tracks line breaks' do
    expect(lex_one('foo')).to include(
      {
        kind: TokenKind::NAME,
        start: 0,
        end: 3,
        line: 1,
        column: 1,
        value: 'foo'
      }
    )
    expect(lex_one("\nfoo")).to include(
      {
        kind: TokenKind::NAME,
        start: 1,
        end: 4,
        line: 2,
        column: 1,
        value: 'foo'
      }
    )
    expect(lex_one("\rfoo")).to include(
      {
        kind: TokenKind::NAME,
        start: 1,
        end: 4,
        line: 2,
        column: 1,
        value: 'foo'
      }
    )
    expect(lex_one("\r\nfoo")).to include(
      {
        kind: TokenKind::NAME,
        start: 2,
        end: 5,
        line: 2,
        column: 1,
        value: 'foo'
      }
    )
    expect(lex_one("\n\rfoo")).to include(
      {
        kind: TokenKind::NAME,
        start: 2,
        end: 5,
        line: 3,
        column: 1,
        value: 'foo'
      }
    )
    expect(lex_one("\r\r\n\nfoo")).to include(
      {
        kind: TokenKind::NAME,
        start: 4,
        end: 7,
        line: 4,
        column: 1,
        value: 'foo'
      }
    )
    expect(lex_one("\n\n\r\rfoo")).to include(
      {
        kind: TokenKind::NAME,
        start: 4,
        end: 7,
        line: 5,
        column: 1,
        value: 'foo'
      }
    )
  end

  it 'records line and column' do
    expect(lex_one("\n \r\n \r  foo\n")).to include(
      {
        kind: TokenKind::NAME,
        start: 8,
        end: 11,
        line: 4,
        column: 3,
        value: 'foo'
      }
    )

    expect_syntax_error("\n\n %\n").to include(
      {
        message: 'Syntax Error: Cannot parse the unexpected character "%".',
        locations: [{ line: 3, column: 2 }]
      }
    )
  end

  it 'skips whitespace and comments' do
    expect(
      lex_one(
        '

    foo


'
      )
    ).to include({ kind: TokenKind::NAME, start: 6, end: 9, value: 'foo' })

    expect(
      lex_one(
        '
    //comment
    foo//comment
'
      )
    ).to include({ kind: TokenKind::NAME, start: 19, end: 22, value: 'foo' })

    expect(lex_one('   foo   ')).to include(
      { kind: TokenKind::NAME, start: 3, end: 6, value: 'foo' }
    )
  end

  it 'errors respect whitespace' do
    expect_syntax_error_string(['', '', '    %', ''].join("\n")).to eq(
      TestUtils.dedent '
      Syntax Error: Cannot parse the unexpected character "%".

      TSJSON Source:3:5
      2 |
      3 |     %
        |     ^
      4 |
    '
    )
  end

  it 'updates line numbers in error for file context' do
    caughtError = nil
    begin
      str = ['', '', '     %', ''].join("\n")
      source = Source.new(str, 'foo.js', { line: 11, column: 12 })
      Lexer.new(source).advance
    rescue => e
      caughtError = e
    end

    expect(caughtError.to_s + "\n").to eq(
      TestUtils.dedent '
      Syntax Error: Cannot parse the unexpected character "%".

      foo.js:13:6
      12 |
      13 |      %
         |      ^
      14 |
    '
    )
  end

  it 'updates column numbers in error for file context' do
    caughtError = nil
    begin
      source = Source.new('%', 'foo.js', { line: 1, column: 5 })
      Lexer.new(source).advance
    rescue => e
      caughtError = e
    end
    expect(caughtError.to_s + "\n").to eq(
      TestUtils.dedent '
      Syntax Error: Cannot parse the unexpected character "%".

      foo.js:1:5
      1 |     %
        |     ^
    '
    )
  end

  it 'lexes strings' do
    expect(lex_one('""')).to include(
      { kind: TokenKind::STRING, start: 0, end: 2, value: '' }
    )

    expect(lex_one('"simple"')).to include(
      { kind: TokenKind::STRING, start: 0, end: 8, value: 'simple' }
    )

    expect(lex_one('" white space "')).to include(
      { kind: TokenKind::STRING, start: 0, end: 15, value: ' white space ' }
    )

    expect(lex_one("\"quote \\\"\"")).to include(
      { kind: TokenKind::STRING, start: 0, end: 10, value: 'quote "' }
    )

    expect(lex_one('"escaped \\n\\r\\b\\t\\f"')).to include(
      {
        kind: TokenKind::STRING,
        start: 0,
        end: 20,
        value: 'escaped \n\r\b\t\f'
      }
    )

    expect(lex_one('"slashes \\\\ \\/"')).to include(
      { kind: TokenKind::STRING, start: 0, end: 15, value: 'slashes \\ /' }
    )

    expect(lex_one('"unicode \\u1234\\u5678\\u90AB\\uCDEF"')).to include(
      {
        kind: TokenKind::STRING,
        start: 0,
        end: 34,
        value: "unicode \u1234\u5678\u90AB\uCDEF"
      }
    )
  end

  it 'lex reports useful string errors' do
    expect_syntax_error('"').to match(
      {
        message: 'Syntax Error: Unterminated string.',
        locations: [{ line: 1, column: 2 }]
      }
    )

    expect_syntax_error('"""').to match(
      {
        message: 'Syntax Error: Unterminated string.',
        locations: [{ line: 1, column: 4 }]
      }
    )

    expect_syntax_error('"no end quote').to match(
      {
        message: 'Syntax Error: Unterminated string.',
        locations: [{ line: 1, column: 14 }]
      }
    )

    expect_syntax_error("'single quotes'").to match(
      {
        message:
          'Syntax Error: Unexpected single quote character (\'), did you mean to use a double quote (")?',
        locations: [{ line: 1, column: 1 }]
      }
    )

    expect_syntax_error("\"contains unescaped \u0007 control char\"").to match(
      {
        message: 'Syntax Error: Invalid character within String: "\\u0007".',
        locations: [{ line: 1, column: 21 }]
      }
    )

    expect_syntax_error("\"null-byte is not \u0000 end of file\"").to match(
      {
        message: 'Syntax Error: Invalid character within String: "\\u0000".',
        locations: [{ line: 1, column: 19 }]
      }
    )

    expect_syntax_error("\"multi\nline\"").to match(
      {
        message: 'Syntax Error: Unterminated string.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"multi\rline\"").to match(
      {
        message: 'Syntax Error: Unterminated string.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\z esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\z.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\x esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\x.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\u1 esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\u1 es.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\u0XX1 esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\u0XX1.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\uXXXX esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\uXXXX.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\uFXXX esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\uFXXX.',
        locations: [{ line: 1, column: 7 }]
      }
    )

    expect_syntax_error("\"bad \\uXXXF esc\"").to match(
      {
        message: 'Syntax Error: Invalid character escape sequence: \\uXXXF.',
        locations: [{ line: 1, column: 7 }]
      }
    )
  end

  it 'lexes numbers' do
    expect(lex_one('4')).to include(
      { kind: TokenKind::INT, start: 0, end: 1, value: '4' }
    )

    expect(lex_one('4.123')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 5, value: '4.123' }
    )

    expect(lex_one('-4')).to include(
      { kind: TokenKind::INT, start: 0, end: 2, value: '-4' }
    )

    expect(lex_one('9')).to include(
      { kind: TokenKind::INT, start: 0, end: 1, value: '9' }
    )

    expect(lex_one('0')).to include(
      { kind: TokenKind::INT, start: 0, end: 1, value: '0' }
    )

    expect(lex_one('-4.123')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 6, value: '-4.123' }
    )

    expect(lex_one('0.123')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 5, value: '0.123' }
    )

    expect(lex_one('123e4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 5, value: '123e4' }
    )

    expect(lex_one('123E4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 5, value: '123E4' }
    )

    expect(lex_one('123e-4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 6, value: '123e-4' }
    )

    expect(lex_one('123e+4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 6, value: '123e+4' }
    )

    expect(lex_one('-1.123e4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 8, value: '-1.123e4' }
    )

    expect(lex_one('-1.123E4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 8, value: '-1.123E4' }
    )

    expect(lex_one('-1.123e-4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 9, value: '-1.123e-4' }
    )

    expect(lex_one('-1.123e+4')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 9, value: '-1.123e+4' }
    )

    expect(lex_one('-1.123e4567')).to include(
      { kind: TokenKind::FLOAT, start: 0, end: 11, value: '-1.123e4567' }
    )
  end

  it 'lex reports useful number errors' do
    expect_syntax_error('00').to include(
      {
        message: 'Syntax Error: Invalid number, unexpected digit after 0: "0".',
        locations: [{ line: 1, column: 2 }]
      }
    )

    expect_syntax_error('01').to include(
      {
        message: 'Syntax Error: Invalid number, unexpected digit after 0: "1".',
        locations: [{ line: 1, column: 2 }]
      }
    )

    expect_syntax_error('01.23').to include(
      {
        message: 'Syntax Error: Invalid number, unexpected digit after 0: "1".',
        locations: [{ line: 1, column: 2 }]
      }
    )

    expect_syntax_error('+1').to include(
      {
        message: 'Syntax Error: Cannot parse the unexpected character "+".',
        locations: [{ line: 1, column: 1 }]
      }
    )

    expect_syntax_error('1.').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: <EOF>.',
        locations: [{ line: 1, column: 3 }]
      }
    )

    expect_syntax_error('1e').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: <EOF>.',
        locations: [{ line: 1, column: 3 }]
      }
    )

    expect_syntax_error('1E').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: <EOF>.',
        locations: [{ line: 1, column: 3 }]
      }
    )

    expect_syntax_error('1.e1').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "e".',
        locations: [{ line: 1, column: 3 }]
      }
    )

    expect_syntax_error('1.A').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "A".',
        locations: [{ line: 1, column: 3 }]
      }
    )

    expect_syntax_error('-A').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "A".',
        locations: [{ line: 1, column: 2 }]
      }
    )

    expect_syntax_error('1.0e').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: <EOF>.',
        locations: [{ line: 1, column: 5 }]
      }
    )

    expect_syntax_error('1.0eA').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "A".',
        locations: [{ line: 1, column: 5 }]
      }
    )

    expect_syntax_error('1.2e3e').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "e".',
        locations: [{ line: 1, column: 6 }]
      }
    )

    expect_syntax_error('1.2e3.4').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: ".".',
        locations: [{ line: 1, column: 6 }]
      }
    )

    expect_syntax_error('1.23.4').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: ".".',
        locations: [{ line: 1, column: 5 }]
      }
    )
  end

  it 'lex does not allow name-start after a number' do
    expect_syntax_error('0xF1').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "x".',
        locations: [{ line: 1, column: 2 }]
      }
    )
    expect_syntax_error('0b10').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "b".',
        locations: [{ line: 1, column: 2 }]
      }
    )
    expect_syntax_error('123abc').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "a".',
        locations: [{ line: 1, column: 4 }]
      }
    )
    expect_syntax_error('1_234').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "_".',
        locations: [{ line: 1, column: 2 }]
      }
    )
    expect_syntax_error('1ß').to include(
      {
        message:
          "Syntax Error: Cannot parse the unexpected character \"\\u00DF\".",
        locations: [{ line: 1, column: 2 }]
      }
    )
    expect_syntax_error('1.23f').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "f".',
        locations: [{ line: 1, column: 5 }]
      }
    )
    expect_syntax_error('1.234_5').to include(
      {
        message: 'Syntax Error: Invalid number, expected digit but got: "_".',
        locations: [{ line: 1, column: 6 }]
      }
    )
    expect_syntax_error('1ß').to include(
      {
        message:
          "Syntax Error: Cannot parse the unexpected character \"\\u00DF\".",
        locations: [{ line: 1, column: 2 }]
      }
    )
  end

  it 'lexes punctuation' do
    expect(lex_one('(')).to include(
      { kind: TokenKind::PAREN_L, start: 0, end: 1 }
    )

    expect(lex_one(')')).to include(
      { kind: TokenKind::PAREN_R, start: 0, end: 1 }
    )

    expect(lex_one(':')).to include(
      { kind: TokenKind::COLON, start: 0, end: 1 }
    )

    expect(lex_one('=')).to include(
      { kind: TokenKind::EQUALS, start: 0, end: 1 }
    )

    expect(lex_one('<')).to include(
      { kind: TokenKind::CHEVRON_L, start: 0, end: 1 }
    )

    expect(lex_one('>')).to include(
      { kind: TokenKind::CHEVRON_R, start: 0, end: 1 }
    )

    expect(lex_one('[')).to include(
      { kind: TokenKind::BRACKET_L, start: 0, end: 1 }
    )

    expect(lex_one(']')).to include(
      { kind: TokenKind::BRACKET_R, start: 0, end: 1 }
    )

    expect(lex_one('{')).to include(
      { kind: TokenKind::BRACE_L, start: 0, end: 1 }
    )

    expect(lex_one('|')).to include({ kind: TokenKind::PIPE, start: 0, end: 1 })

    expect(lex_one('}')).to include(
      { kind: TokenKind::BRACE_R, start: 0, end: 1 }
    )

    expect(lex_one('.')).to include({ kind: TokenKind::DOT, start: 0, end: 1 })
  end

  it 'lex reports useful unknown character error' do
    expect_syntax_error('%%').to include(
      {
        message: 'Syntax Error: Cannot parse the unexpected character "%".',
        locations: [{ line: 1, column: 1 }]
      }
    )

    expect_syntax_error("\u203B").to include(
      {
        message:
          "Syntax Error: Cannot parse the unexpected character \"\\u203B\".",
        locations: [{ line: 1, column: 1 }]
      }
    )

    expect_syntax_error("\u200b").to include(
      {
        message:
          "Syntax Error: Cannot parse the unexpected character \"\\u200B\".",
        locations: [{ line: 1, column: 1 }]
      }
    )
  end

  it 'lex reports useful information for dashes in names' do
    source = Source.new('a-b')
    lexer = Lexer.new(source)
    firstToken = lexer.advance.toJSON
    expect(firstToken).to include(
      { kind: TokenKind::NAME, start: 0, end: 1, value: 'a' }
    )
  end

  it 'produces double linked list of tokens, including comments' do
    source =
      Source.new(
        '
      {
        //comment
        field
      }
    '
      )

    lexer = Lexer.new(source)
    startToken = lexer.token
    endToken = nil
    loop do
      endToken = lexer.advance # easier, but will include them in the linked list result. # Lexer advances over ignored comment tokens to make writing parsers
      expect(endToken.kind).to_not equal(TokenKind::COMMENT)
      break unless (endToken.kind != TokenKind::EOF)
    end

    expect(startToken.prev).to equal(nil)
    expect(endToken.next).to equal(nil)

    tokens = []
    tok = startToken
    loop do
      expect(tok.prev).to match(tokens[tokens.length - 1]) if (tokens.length) # Tokens are double-linked, prev should point to last seen token.
      tokens.push(tok)
      tok = tok.next
      break unless tok
    end

    expect(tokens.map(&:kind)).to match(
      [
        TokenKind::SOF,
        TokenKind::BRACE_L,
        TokenKind::COMMENT,
        TokenKind::NAME,
        TokenKind::BRACE_R,
        TokenKind::EOF
      ]
    )
  end
end

RSpec.describe 'isPunctuatorTokenKind' do
  def is_punctuator_token?(text)
    return LexerUtils.is_punctuator_token_kind?(lex_one(text)[:kind])
  end

  it 'returns true for punctuator tokens' do
    expect(is_punctuator_token?('&')).to equal(true)
    expect(is_punctuator_token?('(')).to equal(true)
    expect(is_punctuator_token?(')')).to equal(true)
    expect(is_punctuator_token?(':')).to equal(true)
    expect(is_punctuator_token?('=')).to equal(true)
    expect(is_punctuator_token?('<')).to equal(true)
    expect(is_punctuator_token?('>')).to equal(true)
    expect(is_punctuator_token?('[')).to equal(true)
    expect(is_punctuator_token?(']')).to equal(true)
    expect(is_punctuator_token?('{')).to equal(true)
    expect(is_punctuator_token?('|')).to equal(true)
    expect(is_punctuator_token?('}')).to equal(true)
  end

  it 'returns false for non-punctuator tokens' do
    expect(is_punctuator_token?('')).to equal(false)
    expect(is_punctuator_token?('name')).to equal(false)
    expect(is_punctuator_token?('1')).to equal(false)
    expect(is_punctuator_token?('3.14')).to equal(false)
    expect(is_punctuator_token?('"str"')).to equal(false)
  end
end
