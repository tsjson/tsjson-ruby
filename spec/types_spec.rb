require './lib/types/index.rb'
include TSJSON

RSpec.describe 'validate' do
  it 'Any' do
    expect(TSJSONAny.validate('test')).to be true
    expect(TSJSONAny.validate(1)).to be true
    expect(TSJSONAny.validate(true)).to be true
    expect(TSJSONAny.validate(nil)).to be true
    expect(TSJSONAny.validate([])).to be true
    expect(TSJSONAny.validate({})).to be true
  end

  it 'String' do
    expect(TSJSONString.validate('test')).to be true
    expect(TSJSONString.valid?(1)).to be false
    expect(TSJSONString.valid?(true)).to be false
    expect(TSJSONString.valid?(nil)).to be false

    expect { TSJSONString.validate(1) }.to raise_error(
      an_instance_of(ScalarValidationError).and(
        having_attributes(
          {
            to_json: {
              code: 'ScalarValidationError',
              details: {
                expected_type: 'String',
                received_type: 'Integer',
                received_value: 1
              }
            }
          }
        )
      )
    )
  end

  it 'Int' do
    expect(TSJSONInt.valid?('test')).to be false
    expect(TSJSONInt.validate(1)).to be true
    expect(TSJSONInt.valid?(1.1)).to be false
    expect(TSJSONInt.valid?(true)).to be false
    expect(TSJSONInt.valid?(nil)).to be false

    expect { TSJSONInt.validate('test') }.to raise_error(
      an_instance_of(ScalarValidationError).and(
        having_attributes(
          {
            to_json: {
              code: 'ScalarValidationError',
              details: {
                expected_type: 'Int',
                received_type: 'String',
                received_value: 'test'
              }
            }
          }
        )
      )
    )
  end

  it 'Float' do
    expect(TSJSONFloat.valid?('test')).to be false
    expect(TSJSONFloat.validate(1)).to be true
    expect(TSJSONFloat.validate(1.1)).to be true
    expect(TSJSONFloat.valid?(true)).to be false
    expect(TSJSONFloat.valid?(nil)).to be false

    expect { TSJSONFloat.validate('test') }.to raise_error(
      an_instance_of(ScalarValidationError).and(
        having_attributes(
          {
            to_json: {
              code: 'ScalarValidationError',
              details: {
                expected_type: 'Float',
                received_type: 'String',
                received_value: 'test'
              }
            }
          }
        )
      )
    )
  end

  it 'Boolean' do
    expect(TSJSONBoolean.valid?('test')).to be false
    expect(TSJSONBoolean.valid?(1)).to be false
    expect(TSJSONBoolean.valid?(1.1)).to be false
    expect(TSJSONBoolean.validate(true)).to be true
    expect(TSJSONBoolean.validate(false)).to be true
    expect(TSJSONBoolean.valid?(nil)).to be false

    expect { TSJSONBoolean.validate('test') }.to raise_error(
      an_instance_of(ScalarValidationError).and(
        having_attributes(
          {
            to_json: {
              code: 'ScalarValidationError',
              details: {
                expected_type: 'Boolean',
                received_type: 'String',
                received_value: 'test'
              }
            }
          }
        )
      )
    )
  end

  context 'Literal' do
    it 'valid' do
      testLiteral = Literal.new('test')

      expect(testLiteral.valid?('test')).to be true
      expect(testLiteral.valid?('new test')).to be false
      expect(testLiteral.valid?(1)).to be false
      expect(testLiteral.valid?(nil)).to be false

      expect { testLiteral.validate(1) }.to raise_error(
        an_instance_of(LiteralValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'LiteralValidationError',
                details: { expected_value: 'test', received_value: 1 }
              }
            }
          )
        )
      )
    end

    it 'compare' do
      expect(
        [Literal.new('str1'), Literal.new('str2')].include?(Literal.new('str1'))
      ).to be true

      expect(
        [Literal.new('str1'), Literal.new('str2')].include?(Literal.new('str3'))
      ).to be false
    end
  end

  context 'Scalar Union' do
    it 'validate' do
      testUnion = ScalarUnion.new([TSJSONString, TSJSONInt])

      expect(testUnion.valid?('test')).to be true
      expect(testUnion.valid?(1)).to be true
      expect(testUnion.valid?(1.1)).to be false
      expect(testUnion.valid?(true)).to be false
      expect(testUnion.valid?(nil)).to be false

      expect { testUnion.validate(true) }.to raise_error(
        an_instance_of(ScalarUnionValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ScalarUnionValidationError',
                details: {
                  expected_types: %w[String Int],
                  received_type: 'TrueClass',
                  received_value: true
                }
              }
            }
          )
        )
      )
    end

    it "can't create union" do
      expect { ScalarUnion.new([Literal.new('Q'), TSJSONInt]) }.to raise_error(
        'ScalarUnion may contain only Scalar types'
      )
    end
  end

  context 'Literal Union' do
    it 'validate' do
      testUnion = LiteralUnion.new([Literal.new('valueA'), Literal.new(1)])

      expect(testUnion.valid?('valueA')).to be true
      expect(testUnion.valid?(1)).to be true
      expect(testUnion.valid?(1.1)).to be false
      expect(testUnion.valid?(true)).to be false
      expect(testUnion.valid?(nil)).to be false

      expect { testUnion.validate(true) }.to raise_error(
        an_instance_of(LiteralUnionValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'LiteralUnionValidationError',
                details: {
                  expected_values: ['valueA', 1],
                  received_value: true
                }
              }
            }
          )
        )
      )
    end

    it "can't create union" do
      expect { LiteralUnion.new([Literal.new('Q'), TSJSONInt]) }.to raise_error(
        'LiteralUnion may contain only Literal types'
      )
    end
  end

  context 'List' do
    it 'with optional item' do
      testList = List.new(ScalarUnion.new([TSJSONString, TSJSONNull]))

      expect(testList.validate(%w[test])).to be true
      expect(testList.valid?([1])).to be false
      expect(testList.validate([nil])).to be true

      expect { testList.validate(1) }.to raise_error(
        an_instance_of(ScalarValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ScalarValidationError',
                details: {
                  expected_type: 'Array',
                  received_type: 'Integer',
                  received_value: 1
                }
              }
            }
          )
        )
      )

      expect { testList.validate(['test', 1]) }.to raise_error(
        an_instance_of(ListValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ListValidationError',
                details: {
                  errors: [
                    {
                      index: 1,
                      error: {
                        code: 'ScalarUnionValidationError',
                        details: {
                          expected_types: %w[String Null],
                          received_type: 'Integer',
                          received_value: 1
                        }
                      }
                    }
                  ]
                }
              }
            }
          )
        )
      )
    end

    it 'with required item' do
      testList = List.new(TSJSONString)

      expect(testList.valid?(%w[test])).to be true
      expect(testList.valid?([1])).to be false
      expect(testList.valid?([nil])).to be false

      expect { testList.validate(['test', 1]) }.to raise_error(
        an_instance_of(ListValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ListValidationError',
                details: {
                  errors: [
                    {
                      index: 1,
                      error: {
                        code: 'ScalarValidationError',
                        details: {
                          expected_type: String,
                          received_type: 'Integer',
                          received_value: 1
                        }
                      }
                    }
                  ]
                }
              }
            }
          )
        )
      )
    end
  end

  context 'Object' do
    it 'optional field' do
      testType =
        ObjectType.new({ title: { type: TSJSONString, optional: true } })

      expect(testType.validate({})).to be true
      expect(testType.validate({ title: 'test' })).to be true
      expect(testType.valid?({ amount: 1 })).to be false

      expect { testType.validate({ title: 1 }) }.to raise_error(
        an_instance_of(ObjectValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ObjectValidationError',
                details: {
                  errors: [
                    {
                      field: 'title',
                      error: {
                        code: 'ScalarValidationError',
                        details: {
                          expected_type: 'String',
                          received_type: 'Integer',
                          received_value: 1
                        }
                      }
                    }
                  ]
                }
              }
            }
          )
        )
      )
    end

    it 'unexpected field' do
      testType =
        ObjectType.new({ title: { type: TSJSONString, optional: true } })

      expect { testType.validate({ amount: 1 }) }.to raise_error(
        an_instance_of(ObjectValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ObjectValidationError',
                details: {
                  errors: [
                    {
                      field: 'amount',
                      error: { code: 'UnexpectedFieldError', details: {} }
                    }
                  ]
                }
              }
            }
          )
        )
      )
    end

    it 'required field' do
      testType = ObjectType.new({ title: { type: TSJSONString } })

      expect { testType.validate({}) }.to raise_error(
        an_instance_of(ObjectValidationError).and(
          having_attributes(
            {
              to_json: {
                code: 'ObjectValidationError',
                details: {
                  errors: [
                    {
                      field: 'title',
                      error: { code: 'RequiredFieldError', details: {} }
                    }
                  ]
                }
              }
            }
          )
        )
      )
    end
  end

  context 'Intersection' do
    it 'A & B' do
      typeA = ObjectType.new({ id: { type: TSJSONInt } })
      typeB = ObjectType.new({ title: { type: TSJSONString } })

      typeC = Intersection.new([typeA, typeB])

      expect(typeC.validate({ id: 1, title: 'test' })).to be true
    end
  end

  context 'Union' do
    it 'A | B' do
      typeA =
        ObjectType.new(
          { type: { type: Literal.new('A') }, value: { type: TSJSONString } }
        )

      typeB =
        ObjectType.new(
          { type: { type: Literal.new('B') }, value: { type: TSJSONInt } }
        )

      testType = Union.new([typeA, typeB])

      expect(testType.validate({ type: 'A', value: 'test' })).to be true
      expect(testType.validate({ type: 'B', value: 1 })).to be true
      expect(testType.valid?({ type: 'A', value: 1 })).to be false
      expect(testType.valid?({ type: 'B', value: 'test' })).to be false
    end

    it '(A & B) | (A & C)' do
      typeA = ObjectType.new({ id: { type: TSJSONInt } })
      typeB =
        ObjectType.new(
          { type: { type: Literal.new('B') }, value: { type: TSJSONString } }
        )

      typeC =
        ObjectType.new(
          { type: { type: Literal.new('C') }, value: { type: TSJSONInt } }
        )

      testType =
        Union.new(
          [Intersection.new([typeA, typeB]), Intersection.new([typeA, typeC])]
        )

      expect(testType.validate({ id: 1, type: 'B', value: 'test' })).to be true
      expect(testType.validate({ id: 2, type: 'C', value: 1 })).to be true
      expect(testType.valid?({ type: 'C', value: 1 })).to be false
      expect(testType.valid?({ type: 'B', value: 1 })).to be false
      expect(testType.valid?({ type: 'C', value: 'test' })).to be false
    end
  end

  context 'Enum' do
    it 'validate' do
      testType = Enum.new({ 'A' => Literal.new('A'), 'B' => Literal.new('B') })

      expect(testType.member('A')).to be_a_kind_of(Literal)
      expect(testType.validate('A')).to be true
      expect(testType.valid?('C')).to be false
    end
  end
end
