require './lib/schema/schema.rb'
include TSJSON

RSpec.describe 'schema' do
  it 'scalar' do
    schema = Schema.build('type TestType = string')
    expect(schema.types_map.keys).to eq %w[TestType]
    expect(schema.type('TestType')).to eq TSJSONString
  end

  it 'array' do
    schema = Schema.build('type TestType = string[]')
    expect(schema.types_map.keys).to eq %w[TestType]
    expect(schema.type('TestType')).to be_a_kind_of(List)
  end

  it 'literal' do
    schema = Schema.build('type TestType = "test"')
    expect(schema.types_map.keys).to eq %w[TestType]
    expect(schema.type('TestType')).to be_a_kind_of(Literal)
    expect(schema.type('TestType').value).to eq ('test')
  end

  context 'object' do
    it 'definition' do
      schema =
        Schema.build(
          'type TestType = {
            f1: string;
            f2?: string;
        }'
        )
      expect(schema.types_map.keys).to eq %w[TestType]

      type = schema.type('TestType')
      expect(type).to be_a_kind_of(ObjectType)
      expect(type.fields).to eq(
        [
          { name: 'f1', type: TSJSONString, optional: false },
          { name: 'f2', type: TSJSONString, optional: true }
        ]
      )
    end
    it 'access field' do
      schema =
        Schema.build(
          '
            type TestObject = {
              f1: string;
              f2?: string;
            }
            
            type TestType = TestObject["f1"]
          '
        )
      expect(schema.types_map.keys).to eq %w[TestObject TestType]

      type = schema.type('TestType')
      expect(type).to be(TSJSONString)
    end
  end

  context 'enum' do
    it 'without values' do
      schema =
        Schema.build(
          'enum TestType {
          A,
          B 
        }'
        )
      expect(schema.types_map.keys).to eq %w[TestType]

      type = schema.type('TestType')
      expect(type).to be_a_kind_of(Enum)
      expect(type.member('A')).to be_a_kind_of(Literal)
      expect(type.member('A').valid?('A')).to be true
    end

    it 'with values' do
      schema =
        Schema.build(
          'enum TestType {
          A = "a",
          B = "b" 
        }'
        )
      expect(schema.types_map.keys).to eq %w[TestType]

      type = schema.type('TestType')
      expect(type).to be_a_kind_of(Enum)
      expect(type.member('A')).to be_a_kind_of(Literal)
      expect(type.member('A').value).to eq 'a'
      expect(type.member('B')).to be_a_kind_of(Literal)
      expect(type.member('B').value).to eq 'b'
    end

    it 'access member' do
      schema =
        Schema.build(
          'enum TestEnum {
            A = "a",
            B = "b" 
          }
          
          type TestType=TestEnum.A
        '
        )
      expect(schema.types_map.keys).to eq %w[TestEnum TestType]

      type = schema.type('TestType')
      expect(type).to be_a_kind_of(Literal)
      expect(type.value).to eq 'a'
    end
  end

  it 'undefined type' do
    expect do
      Schema.build(
        'type TestType = {
            f: RefType;
        }'
      )
    end.to raise_error("Type doesn't exist: RefType")
  end

  it 'circular dependency' do
    schema =
      Schema.build(
        'type TypeA = {
          f?: TypeB;
        }

        type TypeB = {
          f?: TypeA;
        }
        '
      )
    expect(schema.types_map.keys).to eq %w[TypeA TypeB]

    typeA = schema.type('TypeA')
    typeB = schema.type('TypeB')

    expect(typeA.fields).to eq([{ name: 'f', type: typeB, optional: true }])
    expect(typeB.fields).to eq([{ name: 'f', type: typeA, optional: true }])
  end

  it 'intersection' do
    schema =
      Schema.build(
        'type A = { f1: string; }
         type B = { f2?: string; }
         type C = A & B
        '
      )
    expect(schema.types_map.keys).to eq %w[A B C]

    typeA = schema.type('A')
    typeB = schema.type('B')
    typeC = schema.type('C')

    expect(typeC).to be_a_kind_of(Intersection)
    expect(typeC.types).to eq [typeA, typeB]
  end

  it 'union' do
    schema =
      Schema.build(
        'type A = { f1: string; d: "typeA"; }
         type B = { f2?: string; d: "typeB"; }
         type C = A | B
        '
      )
    expect(schema.types_map.keys).to eq %w[A B C]

    typeA = schema.type('A')
    typeB = schema.type('B')
    typeC = schema.type('C')

    expect(typeC).to be_a_kind_of(Union)
    expect(typeC.types).to eq [typeA, typeB]
  end

  it 'scalar union' do
    schema = Schema.build('type A = string | int')
    expect(schema.types_map.keys).to eq %w[A]

    typeA = schema.type('A')

    expect(typeA).to be_a_kind_of(ScalarUnion)
  end

  it 'parentheses' do
    schema =
      Schema.build(
        'type A = { f1: string; d: "typeA"; }
         type B = { f2?: string; d: "typeB"; }
         type C = { f3?: string; }

         type D = (A | B) & C
        '
      )
    expect(schema.types_map.keys).to eq %w[A B C D]

    typeA = schema.type('A')
    typeB = schema.type('B')
    typeC = schema.type('C')
    typeD = schema.type('D')

    expect(typeD).to be_a_kind_of(Intersection)
  end

  it 'generic' do
    schema =
      Schema.build(
        'type gA<T> = { f: T; }
         type gB<T> = T
         type A = gA<string>
         type B = gB<gA<int>>

         type gC<T> = gB<T>
         type C = gC<int>

         type gList<T> = T[]
         type L = gList<int>
        '
      )
    expect(schema.types_map.keys).to eq %w[A B C L]
    expect(schema.type('A').fields[0][:type]).to eq TSJSONString
    expect(schema.type('B').fields[0][:type]).to eq TSJSONInt
    expect(schema.type('C')).to eq TSJSONInt

    expect(schema.type('L')).to be_a_kind_of(List)
    expect(schema.type('L').of_type).to eq TSJSONInt
  end

  it 'not enough discriminators error' do
    expect do
      Schema.build(
        '
        type TypeA = { f: string; }
        type TypeB = { f: string; }
        type TypeC = TypeA | TypeB
        '
      )
    end.to raise_error(an_instance_of(NotEnoughDiscriminators))
  end

  it 'multiple sources' do
    schema = Schema.build(['type B = string', 'type A = B'])
    expect(schema.type('A')).to eq(TSJSONString)
  end
end
