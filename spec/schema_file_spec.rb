require './lib/schema/schema.rb'

RSpec.describe 'schema_file' do
  schema = nil
  before { schema = TSJSON::Schema.build(File.read('./spec/schema.tsjson')) }

  def validation_error_fields(type, object)
    type.validate(object)
  rescue TSJSON::ObjectValidationError => e
    e.to_json[:details][:errors]&.map { |err| err[:field] }
  end

  def validation_human_error(type, object)
    type.validate(object)
  rescue TSJSON::ValidationError => e
    e.to_human_json
  end

  it "Can't distinguish type Post" do
    expect(validation_human_error(schema.type('Post'), {})).to eq(
      {
        message: "Can't distinguish type. Need more discriminators",
        details: [{ message: 'type: text_post, poll_post, event' }]
      }
    )
  end

  it "Can't distinguish type PostIndexResponse" do
    expect(
      validation_human_error(schema.type('PostIndexResponse'), { data: [{}] })
    ).to eq(
      {
        message: 'Invalid object',
        details: [
          {
            message: 'data: One or more items are invalid',
            details: [
              {
                message: "0: Can't distinguish type. Need more discriminators",
                details: [{ message: 'type: text_post, poll_post, event' }]
              }
            ]
          }
        ]
      }
    )
  end

  it 'Unexpected value' do
    expect(
      validation_human_error(
        schema.type('PostIndexResponse'),
        { data: { type: 'text_post' } }
      )
    ).to eq(
      {
        message: 'Invalid object',
        details: [
          {
            message:
              "data: Expected type `Array`. Received value `{:type=>\"text_post\"}` of type `Hash`"
          }
        ]
      }
    )
  end

  it 'Unexpected value' do
    expect(validation_human_error(schema.type('Post'), { type: 'text' })).to eq(
      {
        message:
          "Field 'type' received unexpected value 'text'. Expected values are: text_post, poll_post, event"
      }
    )

    expect(
      validation_human_error(
        schema.type('PostIndexResponse'),
        { data: [{ type: 'text' }, { type: 'poll' }] }
      )
    ).to eq(
      {
        message: 'Invalid object',
        details: [
          {
            message: 'data: One or more items are invalid',
            details: [
              {
                message:
                  "0: Field 'type' received unexpected value 'text'. Expected values are: text_post, poll_post, event"
              },
              {
                message:
                  "1: Field 'type' received unexpected value 'poll'. Expected values are: text_post, poll_post, event"
              }
            ]
          }
        ]
      }
    )
  end

  it 'Invalid object' do
    expect(
      validation_human_error(schema.type('Post'), { type: 'poll_post' })
    ).to eq(
      {
        message: 'Invalid object',
        details: [
          {
            message:
              'Required fields: id, created_at, updated_at, title, content, options'
          }
        ]
      }
    )

    expect(
      validation_human_error(schema.type('Post'), { type: 'event' })
    ).to eq(
      {
        message: 'Invalid object',
        details: [
          {
            message:
              'Required fields: id, created_at, updated_at, title, content'
          }
        ]
      }
    )

    expect(
      validation_human_error(
        schema.type('Post'),
        { id: 1, type: 'event', created_at: 1, updated_at: 1, title: 'test' }
      )
    ).to eq(
      {
        message: 'Invalid object',
        details: [{ message: 'Required fields: content' }]
      }
    )

    expect(
      validation_human_error(
        schema.type('Post'),
        { id: '1', type: 'event', created_at: 1, updated_at: 1, title: 'test' }
      )
    ).to eq(
      {
        message: 'Invalid object',
        details: [
          { message: 'Required fields: content' },
          {
            message:
              'id: Expected type `Int`. Received value `1` of type `String`'
          }
        ]
      }
    )
  end

  it 'success' do
    expect(
      schema
        .type('Post')
        .validate(
          {
            id: 1,
            type: 'event',
            created_at: 1,
            updated_at: 1,
            title: 'test',
            content: 'test'
          }
        )
    ).to be true

    expect(
      schema
        .type('PostIndexResponse')
        .validate(
          {
            data: [
              {
                id: 1,
                type: 'event',
                created_at: 1,
                updated_at: 1,
                title: 'test',
                content: 'test'
              }
            ]
          }
        )
    ).to be true
  end

  context 'SearchItem' do
    it 'Can\'t distinguish type' do
      expect(validation_human_error(schema.type('SearchItem'), {})).to eq(
        {
          message: "Can't distinguish type. Need more discriminators",
          details: [
            { message: 'type: text_post, poll_post, event' },
            { message: '__typename: post, user' }
          ]
        }
      )

      expect(
        validation_human_error(
          schema.type('SearchItem'),
          { __typename: 'post' }
        )
      ).to eq(
        {
          message: "Can't distinguish type. Need more discriminators",
          details: [{ message: 'type: text_post, poll_post, event' }]
        }
      )
    end

    it 'Invalid object' do
      expect(
        validation_human_error(schema.type('SearchItem'), { type: 'event' })
      ).to eq(
        {
          message: 'Invalid object',
          details: [
            {
              message:
                'Required fields: id, created_at, updated_at, title, content, __typename'
            }
          ]
        }
      )
    end

    it 'Success' do
      expect(
        schema.type('SearchItem').validate({ __typename: 'user' })
      ).to be true
    end
  end
end
