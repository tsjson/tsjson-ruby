require './lib/language/parser/parser.rb'
include TSJSON

RSpec.describe 'parser' do
  it 'Document' do
    p =
      Parser.new(
        '
          type TestA = String
          type TestB = Int
        '
      )
    ast = p.parse_document
    expect(ast[:kind]).to eq('Document')
    expect(ast[:definitions].length).to be(2)
  end
end

RSpec.describe 'Definition' do
  def parse_definition(source)
    p = Parser.new(source)
    p.expect_token(TokenKind::SOF)
    type = p.parse_definition
    p.expect_token(TokenKind::EOF)
    return type
  end

  it 'TypeAlias' do
    expect(parse_definition('type A = B')[:kind]).to eq('TypeAlias')
    expect(parse_definition('type A = B;')[:kind]).to eq('TypeAlias')
  end

  it 'Enum' do
    expect(parse_definition('enum Enum { A, B, C }')[:kind]).to eq('Enum')
    expect(parse_definition('enum Enum { A, B, C };')[:kind]).to eq('Enum')
  end
end

RSpec.describe 'Type definition' do
  def parse_type_definition(source)
    p = Parser.new(source)
    p.expect_token(TokenKind::SOF)
    type = p.parse_operation
    p.expect_token(TokenKind::EOF)
    return type
  end

  it 'TypeReference' do
    expect(parse_type_definition('String')[:kind]).to eq('TypeReference')
  end

  it 'TypeReference access property' do
    ast = parse_type_definition('Type.property')

    expect(ast[:kind]).to eq('PropertyAccess')
    expect(ast[:property][:value]).to eq('property')
    expect(ast[:target][:kind]).to eq('TypeReference')
  end

  it 'TypeReference access index' do
    ast = parse_type_definition('Type["property"]')

    expect(ast[:kind]).to eq('IndexAccess')
    expect(ast[:index][:value]).to eq('property')
    expect(ast[:target][:kind]).to eq('TypeReference')
  end

  it 'Chain of property and index accessors' do
    expect(
      parse_type_definition('Type["property"]["property"].property.property')[
        :kind
      ]
    ).to eq('PropertyAccess')

    expect(
      parse_type_definition('Type.property.property["property"]["property"]')[
        :kind
      ]
    ).to eq('IndexAccess')
  end

  it 'StringLiteral' do
    expect(parse_type_definition('"String"')[:kind]).to eq('StringLiteral')
  end

  it 'Int' do
    ast = parse_type_definition('1')
    expect(ast[:kind]).to eq('Int')
    expect(ast[:value]).to eq(1)
  end

  it 'Float' do
    ast = parse_type_definition('1.1')
    expect(ast[:kind]).to eq('Float')
    expect(ast[:value]).to eq(1.1)
  end

  it 'Literal union' do
    expect(parse_type_definition('"A" | "B"')[:kind]).to eq('UnionType')
    expect(parse_type_definition('1 | 2')[:kind]).to eq('UnionType')
  end

  it 'ArrayType' do
    expect(parse_type_definition('String[]')[:kind]).to eq('ArrayType')
    expect(parse_type_definition('(String|Int)[]')[:kind]).to eq('ArrayType')
  end

  it 'Two-dimensional array' do
    ast = parse_type_definition('String[][]')
    expect(ast[:kind]).to eq('ArrayType')
    expect(ast[:type][:kind]).to eq('ArrayType')
  end

  it 'TypeLiteral' do
    expect(parse_type_definition('{ f: Type; }')[:kind]).to eq('TypeLiteral')
  end

  it 'ParenthesizedType' do
    expect(parse_type_definition('(A)')[:kind]).to eq('ParenthesizedType')
    expect(parse_type_definition('(A & B)')[:kind]).to eq('ParenthesizedType')
  end

  it 'UnionType' do
    expect(parse_type_definition('A | B & C')[:kind]).to eq('UnionType')
    expect(parse_type_definition('A & B | C')[:kind]).to eq('UnionType')
    expect(parse_type_definition('A | (B & C)')[:kind]).to eq('UnionType')
    expect(
      parse_type_definition(
        '
      | A 
      | B'
      )[
        :kind
      ]
    ).to eq('UnionType')
  end

  it 'IntersectionType' do
    expect(parse_type_definition('A & B')[:kind]).to eq('IntersectionType')
    expect(parse_type_definition('A & (B | C)')[:kind]).to eq(
      'IntersectionType'
    )
  end

  it 'Tuple' do
    expect(parse_type_definition('[String, Number]')[:kind]).to eq('Tuple')
  end
end

RSpec.describe 'Type alias' do
  def parse_type_alias(source)
    p = Parser.new(source)
    p.expect_token(TokenKind::SOF)
    type = p.parse_type_alias_definition
    p.expect_token(TokenKind::EOF)
    return type
  end

  it 'Simple assignment' do
    ast = parse_type_alias('type A = B')
    expect(ast[:kind]).to eq('TypeAlias')
    expect(ast[:name][:value]).to eq('A')
  end

  it 'Generic with one parameter' do
    ast = parse_type_alias('type A<T1> = B')
    expect(ast[:kind]).to eq('TypeAlias')
    expect(ast[:parameters][0][:name][:value]).to eq('T1')
  end

  it 'Generic with three parameters' do
    ast = parse_type_alias('type A<T1, T2, T3> = B')
    expect(ast[:kind]).to eq('TypeAlias')
    expect(ast[:parameters][0][:name][:value]).to eq('T1')
    expect(ast[:parameters][2][:name][:value]).to eq('T3')
  end
end

RSpec.describe 'TypeReference' do
  def parse_type_reference(source)
    p = Parser.new(source)
    p.expect_token(TokenKind::SOF)
    type = p.parse_type_reference
    p.expect_token(TokenKind::EOF)
    return type
  end

  it 'Simple' do
    ast = parse_type_reference('A')
    expect(ast[:name][:value]).to eq('A')
  end

  it 'Generic' do
    ast = parse_type_reference('A<T>')
    expect(ast[:name][:value]).to eq('A')
    expect(ast[:args][0][:kind]).to eq('TypeReference')
    expect(ast[:args][0][:name][:value]).to eq('T')
  end

  it 'Generic with two arguments' do
    ast = parse_type_reference('A<T1, T2>')
    expect(ast[:name][:value]).to eq('A')

    expect(ast[:args][0][:kind]).to eq('TypeReference')
    expect(ast[:args][0][:name][:value]).to eq('T1')

    expect(ast[:args][1][:kind]).to eq('TypeReference')
    expect(ast[:args][1][:name][:value]).to eq('T2')
  end
end

RSpec.describe 'TypeLiteral' do
  def parse_type_literal(source)
    p = Parser.new(source)
    p.expect_token(TokenKind::SOF)
    type = p.parse_type_literal
    p.expect_token(TokenKind::EOF)
    return type
  end

  it 'Simple' do
    ast = parse_type_literal('{ field: T; }')
    expect(ast[:properties][0][:name][:value]).to eq('field')
    expect(ast[:properties][0][:type]).to include(
      { kind: AST::Kind::TypeReference }
    )
    expect(ast[:properties][0][:type][:name]).to include({ value: 'T' })
  end

  it 'No fields' do
    ast = parse_type_literal('{ }')
    expect(ast[:properties].length).to be(0)
  end

  it 'Optional' do
    ast = parse_type_literal('{ field?: T; }')
    expect(ast[:properties][0]).to include({ optional: true })
  end

  it 'Two fields' do
    ast =
      parse_type_literal(['{', 'field1: T1;', 'field2: T2;', '}'].join("\n"))
    expect(ast[:properties][0][:name][:value]).to eq('field1')
    expect(ast[:properties][1][:name][:value]).to eq('field2')
  end

  it 'With arguments' do
    ast = parse_type_literal('{ field?: T<T1, T2>; }')
    expect(ast[:properties][0][:type][:kind]).to eq(AST::Kind::TypeReference)
  end

  it 'Operation' do
    ast = parse_type_literal('{ field: A | B; }')
    expect(ast[:properties][0][:type][:kind]).to eq(AST::Kind::UnionType)
  end
end

RSpec.describe 'Enum' do
  def parse_enum_definition(source)
    p = Parser.new(source)
    p.expect_token(TokenKind::SOF)
    type = p.parse_enum_definition
    p.expect_token(TokenKind::EOF)
    return type
  end

  it 'Simple' do
    ast = parse_enum_definition('enum E { A, B }')
    expect(ast[:members][0][:name][:value]).to eq('A')
    expect(ast[:members][1][:name][:value]).to eq('B')
  end

  it 'With values' do
    ast =
      parse_enum_definition(
        'enum E { 
      A = "a", 
      B = "b" 
    }'
      )

    expect(ast[:members][0][:name][:value]).to eq('A')
    expect(ast[:members][0][:value][:value]).to eq('a')
    expect(ast[:members][1][:name][:value]).to eq('B')
    expect(ast[:members][1][:value][:value]).to eq('b')
  end
end
