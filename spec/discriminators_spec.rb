require './lib/types/index.rb'
include TSJSON

RSpec.describe 'discriminators' do
  context 'nesting' do
    typeA =
      ObjectType.new(
        {
          f8: { type: Literal.new('1/8'), optional: true },
          f4: { type: Literal.new('1/4'), optional: true },
          f2: { type: Literal.new('1/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('str'), optional: true },
          value: { type: TSJSONString, optional: true }
        }
      )

    typeB =
      ObjectType.new(
        {
          f8: { type: Literal.new('2/8'), optional: true },
          f4: { type: Literal.new('1/4'), optional: true },
          f2: { type: Literal.new('1/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('num'), optional: true },
          value: { type: TSJSONFloat, optional: true }
        }
      )

    typeC =
      ObjectType.new(
        {
          f8: { type: Literal.new('3/8'), optional: true },
          f4: { type: Literal.new('2/4'), optional: true },
          f2: { type: Literal.new('1/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('bool'), optional: true },
          value: { type: TSJSONBoolean, optional: true }
        }
      )

    typeD =
      ObjectType.new(
        {
          f8: { type: Literal.new('4/8'), optional: true },
          f4: { type: Literal.new('2/4'), optional: true },
          f2: { type: Literal.new('1/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('str'), optional: true },
          value: { type: TSJSONString, optional: true }
        }
      )

    typeE =
      ObjectType.new(
        {
          f8: { type: Literal.new('5/8'), optional: true },
          f4: { type: Literal.new('3/4'), optional: true },
          f2: { type: Literal.new('2/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('num'), optional: true },
          value: { type: TSJSONFloat, optional: true }
        }
      )

    typeF =
      ObjectType.new(
        {
          f8: { type: Literal.new('6/8'), optional: true },
          f4: { type: Literal.new('3/4'), optional: true },
          f2: { type: Literal.new('2/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('bool'), optional: true },
          value: { type: TSJSONBoolean, optional: true }
        }
      )

    typeG =
      ObjectType.new(
        {
          f8: { type: Literal.new('7/8'), optional: true },
          f4: { type: Literal.new('4/4'), optional: true },
          f2: { type: Literal.new('2/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('str'), optional: true },
          value: { type: TSJSONString, optional: true }
        }
      )

    typeH =
      ObjectType.new(
        {
          f8: { type: Literal.new('8/8'), optional: true },
          f4: { type: Literal.new('4/4'), optional: true },
          f2: { type: Literal.new('2/2'), optional: true },
          f1: { type: Literal.new('1/1'), optional: true },
          type: { type: Literal.new('num'), optional: true },
          value: { type: TSJSONFloat, optional: true }
        }
      )

    testUnion =
      Union.new([typeA, typeB, typeC, typeD, typeE, typeF, typeG, typeH])

    it 'map' do
      map = DiscriminatorMap.new(testUnion.types)

      expect(map.discriminators.map { |d| d[:field].to_s }).to eq %w[
           f8
           f4
           type
           f2
         ]
      expect(
        map['f2']['1/2'].discriminators.map { |d| d[:field].to_s }
      ).to eq %w[f8 type f4]

      expect(
        map['f4']['1/4'].discriminators.map { |d| d[:field].to_s }
      ).to eq %w[f8 type]

      expect(
        map['type']['str'].discriminators.map { |d| d[:field].to_s }
      ).to eq %w[f8 f4 f2]

      expect(map['f8']['2/8']).to be typeB

      expect(map['f4']['2/4']['type']['bool']).to be typeC
    end

    it 'unexpected value exception' do
      expect { testUnion.validate({ type: 'test' }) }.to raise_error(
        an_instance_of(UnexpectedValueError).and(
          having_attributes(
            {
              to_json: {
                code: 'UnexpectedValueError',
                details: {
                  field: 'type',
                  expected_values: %w[str num bool],
                  received: 'test'
                }
              }
            }
          )
        )
      )
    end

    it 'can\'t distinguish type exception' do
      expect { testUnion.validate({ f4: '3/4' }) }.to raise_error(
        an_instance_of(CantDistinguishTypeError).and(
          having_attributes(
            {
              to_json: {
                code: 'CantDistinguishTypeError',
                details: {
                  discriminators: [
                    { field: 'f8', values: %w[5/8 6/8] },
                    { field: 'type', values: %w[num bool] }
                  ]
                }
              }
            }
          )
        )
      )
    end

    it 'ok' do
      expect(testUnion.validate({ f4: '3/4', type: 'num' })).to be true
    end
  end

  context 'Can\'t build map >' do
    t1 =
      ObjectType.new(
        {
          type: { type: Literal.new('str'), optional: true },
          value: { type: TSJSONString, optional: true }
        }
      )

    t2 =
      ObjectType.new(
        {
          type: { type: Literal.new('str'), optional: true },
          value: { type: TSJSONString, optional: true }
        }
      )

    t3 =
      ObjectType.new(
        {
          type: { type: Literal.new('num'), optional: true },
          value: { type: TSJSONInt, optional: true }
        }
      )

    t4 = ObjectType.new({ value: { type: TSJSONInt, optional: true } })

    t5 = ObjectType.new({ value: { type: TSJSONInt, optional: true } })

    it 'all types have the same discriminator' do
      expect { DiscriminatorMap.new([t1, t2]) }.to raise_error(
        an_instance_of(NotEnoughDiscriminators)
      )
    end

    it 'two types have the same discriminator' do
      expect { DiscriminatorMap.new([t1, t2, t3]) }.to raise_error(
        an_instance_of(NotEnoughDiscriminators)
      )
    end

    it 'types have no discriminators' do
      expect { DiscriminatorMap.new([t4, t5]) }.to raise_error(
        an_instance_of(NotEnoughDiscriminators)
      )
    end

    it 'one of the types have no discriminators' do
      expect { DiscriminatorMap.new([t1, t5]) }.to raise_error(
        an_instance_of(NotEnoughDiscriminators)
      )
    end
  end

  context 'Literal union' do
    it 'valid' do
      typeA =
        ObjectType.new(
          {
            type: {
              type:
                LiteralUnion.new([Literal.new('str'), Literal.new('string')])
            },
            value: { type: TSJSONString, optional: true }
          }
        )

      typeB =
        ObjectType.new(
          {
            type: { type: Literal.new('num') },
            value: { type: TSJSONInt, optional: true }
          }
        )

      expect { DiscriminatorMap.new([typeA, typeB]) }.not_to raise_error
    end
  end
end
