module TSJSON
  class ScalarUnion < Base
    attr_reader :types

    def initialize(types = [])
      types.each do |t|
        unless t.is_a?(ScalarType)
          raise 'ScalarUnion may contain only Scalar types'
        end
      end

      @types = types
    end

    def validate(value)
      @types.each { |type| return true if type.valid?(value) }

      raise ScalarUnionValidationError.new(
              expected_types: @types.map(&:name),
              received_type: value.class.name,
              received_value: value
            )
    end
  end
end
