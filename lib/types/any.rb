require_relative './scalar.rb'

module TSJSON
  class AnyType < ScalarType
    def initialize
      super('Any')
    end

    def validate(value)
      true
    end
  end

  TSJSONAny = AnyType.new
end
