module TSJSON
  class LiteralUnion < Base
    attr_reader :types

    def initialize(types = [])
      types.each { |t| validate_type!(t) }

      @types = types
    end

    def push_type(type)
      validate_type!(type)
      return self if types.include?(type)
      return LiteralUnion.new(@types.dup.push(type))
    end

    def has?(type)
      @types.include?(type)
    end

    def size
      @types.size
    end

    def validate_type!(type)
      unless type.is_a?(Literal)
        raise 'LiteralUnion may contain only Literal types'
      end
    end

    def validate(value)
      @types.each { |type| return true if type.valid?(value) }

      raise LiteralUnionValidationError.new(
              expected_values: @types.map(&:value),
              received_value: value
            )
    end

    def to_s
      @types.map(&:to_s).join(' | ')
    end

    def to_json
      @types.map(&:value)
    end
  end
end
