module TSJSON
  class Enum < Base
    def initialize(members = {})
      super()

      @members = members
      @members.each { |name, type| validate_type!(type) }
    end

    def member(name)
      m = @members[name]
      raise "Can't access property #{name} of #{self.class.name}" unless m

      m
    end
    alias property member

    def push_type(type)
      validate_type!(type)
      return self if types.include?(type)
      return Enum.new(@types.dup.push(type))
    end

    def types
      @types ||= @members.values
    end

    def has?(type)
      types.include?(type)
    end

    def validate_type!(type)
      unless type.is_a?(Literal)
        raise "Enum can contain only literal values received #{type.class}"
      end
    end

    def validate(value)
      types.each { |type| return true if type.valid?(value) }

      raise LiteralUnionValidationError.new(
              expected_values: types.map(&:value),
              received_value: value
            )
    end
  end
end
