module TSJSON
  class Merge < Base
    attr_reader :types
    attr_accessor :is_normal

    def initialize(types = [], is_normal = false)
      super()

      types.each do |t|
        unless t.is_a?(ObjectType) || t.is_a?(Union) || t.is_a?(Intersection)
          raise "#{
                  self.class.name
                } may contain only ObjectType, Union or Intersection types"
        end
      end

      @types = types
      @is_normal = types.length == 0 || is_normal
    end
  end
end
