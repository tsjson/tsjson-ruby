module TSJSON
  class List < Base
    def initialize(type)
      super()
      @type = type
    end

    def of_type
      @type
    end

    def validate(value)
      unless value.is_a?(::Array)
        raise ScalarValidationError.new(
                expected_type: 'Array',
                received_type: value.class.name,
                received_value: value
              )
      end

      errors = []
      value.each_with_index do |item, index|
        of_type.validate(item)
      rescue ValidationError => e
        errors.push({ index: index, error: e })
      end

      raise ListValidationError.new(errors: errors) if errors.length > 0

      true
    end
  end
end
