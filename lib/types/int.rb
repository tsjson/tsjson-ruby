require_relative './scalar.rb'

module TSJSON
  class IntType < ScalarType
    def initialize
      super('Int')
    end

    def validate(value)
      super(value) unless value.is_a?(::Integer)

      true
    end
  end

  TSJSONInt = IntType.new
end
