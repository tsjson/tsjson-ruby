module TSJSON
  class ObjectType < Base
    def initialize(fields_map = nil, &block)
      super()
      @fields_map = fields_map&.transform_keys!(&:to_s)
      @resolve_fields = block
    end

    def fields
      @fields = fields_map.map { |k, v| v.merge({ name: k }) } unless @fields

      @fields
    end

    def field(name)
      f = fields_map.dig(name, :type)
      raise "Can't access index #{name} of #{self.class.name}" unless f

      f
    end
    alias index field

    def fields_map
      unless @fields_map
        @fields_map = @resolve_fields.call&.transform_keys!(&:to_s)
      end
      @fields_map
    end

    def compile
      return if @compiled
      @compiled = true

      fields.each { |f| f[:type].compile }
    end

    def validate(object)
      unless object.is_a?(::Hash)
        raise ScalarValidationError.new(
                expected_type: 'Object',
                received_type: object.class.name,
                received_value: object
              )
      end

      object = object&.transform_keys!(&:to_s)

      keys = object.keys
      errors = []

      fields.each do |f|
        name = f[:name]
        type = f[:type]
        optional = f[:optional]
        value = object[name]

        keys.delete(name)
        has_field = object.key?(name)
        next if optional && !has_field
        raise RequiredFieldError.new if !optional && !has_field

        type.validate(value)
      rescue ValidationError => e
        errors.push({ field: name, error: e })
      end
      keys.each do |name|
        errors.push({ field: name, error: UnexpectedFieldError.new })
      end

      raise ObjectValidationError.new(errors: errors) if errors.length > 0

      true
    end

    def to_s
      "{\n#{
        fields.map do |f|
          name = f[:name]
          type = f[:type].to_s
          optional = f[:optional]

          "  #{name}#{optional ? '?' : ''}: #{type}"
        end.join("\n")
      }\n}"
    end
  end
end
