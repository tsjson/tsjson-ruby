require_relative './merge.rb'
require_relative './discriminator_map.rb'

module TSJSON
  class Union < Merge
    def normal_form
      unless @normalized
        @normalized =
          @types.reduce(Union.new) do |buffer, type|
            if type.is_a?(Merge)
              buffer.union_normal(type.normal_form)
            else
              buffer.union_normal(type)
            end
          end
      end
      @normalized
    end

    def intersect_normal(other)
      #   raise_if_not_composite(other)
      @types.reduce(Union.new) do |u, t|
        u.union_normal(t.intersect_normal(other))
      end
    end

    def union_normal(other)
      #   raise_if_not_composite(other)

      if other.is_a?(Union)
        Union.new(@types.dup.concat(other.types), true)
      else
        Union.new(@types.dup.push(other), true)
      end
    end

    def validate(object)
      return normal_form.validate(object) unless @is_normal

      discriminator_map.validate(object)
    end

    def to_s
      @types.map(&:to_s).join(' | ')
    end

    def compile
      return if @compiled
      @compiled = true

      return normal_form.compile unless @is_normal
      discriminator_map
    end

    private

    def discriminator_map
      @discriminator_map ||= DiscriminatorMap.new(@types)
    end
  end
end
