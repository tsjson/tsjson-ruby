require_relative './scalar.rb'

module TSJSON
  class BooleanType < ScalarType
    def initialize
      super('Boolean')
    end

    def validate(value)
      super(value) unless value == true || value == false

      true
    end
  end

  TSJSONBoolean = BooleanType.new
end
