require_relative './base.rb'

module TSJSON
  class ScalarType < Base
    attr_reader :name

    def initialize(name)
      super()
      @name = name
    end

    def validate(value)
      raise ScalarValidationError.new(
              expected_type: @name,
              received_type: value.class.name,
              received_value: value
            )
    end

    def to_s
      @name
    end
  end
end
