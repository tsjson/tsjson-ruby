require_relative './scalar.rb'

module TSJSON
  class NullType < ScalarType
    def initialize
      super('Null')
    end

    def validate(value)
      super(value) unless value == nil

      true
    end
  end

  TSJSONNull = NullType.new
end
