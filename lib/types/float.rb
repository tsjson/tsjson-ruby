require_relative './scalar.rb'

module TSJSON
  class FloatType < ScalarType
    def initialize
      super('Float')
    end

    def validate(value)
      super(value) unless value.is_a?(::Numeric)

      true
    end
  end

  TSJSONFloat = FloatType.new
end
