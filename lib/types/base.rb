module TSJSON
  class Base
    def valid?(value)
      validate(value)
    rescue ValidationError
      false
    end

    def compile; end

    def property(name)
      raise "Can't access property #{name} of #{self.class.name}"
    end

    def index(name)
      raise "Can't access index #{name} of #{self.class.name}"
    end
  end
end
