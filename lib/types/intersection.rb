require_relative './merge.rb'

module TSJSON
  class Intersection < Merge
    def normal_form
      unless @normalized
        @normalized =
          @types.reduce(Intersection.new) do |buffer, type|
            if type.is_a?(Merge)
              buffer.intersect_normal(type.normal_form)
            else
              buffer.intersect_normal(type)
            end
          end
      end
      @normalized
    end

    def intersect_normal(other)
      #   raise_if_not_composite(other)

      if other.is_a?(Intersection)
        Intersection.new(@types.dup.concat(other.types), true)
      elsif other.is_a?(Union)
        other.types.reduce(Union.new) do |u, t|
          u.union_normal(intersect_normal(t))
        end
      else
        Intersection.new(@types.dup.push(other), true)
      end
    end

    def union_normal(other)
      #   raise_if_not_composite(other)

      Union.new([self, other], true)
    end

    def validate(object)
      return normal_form.validate(object) unless @is_normal

      merged_object.validate(object)
    end

    def fields
      merged_object.fields
    end

    def to_s
      @types.map(&:to_s).join(' & ')
    end

    def compile
      return if @compiled
      @compiled = true

      return normal_form.compile unless @is_normal
      merged_object
    end

    private

    def merged_object
      raise 'not a normal form' unless @is_normal

      @merged_object ||=
        ObjectType.new(
          @types.reduce({}) { |obj, type| obj.merge(type.fields_map) }
        )
    end
  end
end
