module TSJSON
  class Literal < Base
    attr_reader :value

    def initialize(value)
      @value = value
    end

    def ==(other)
      return value == other unless other.is_a?(self.class)

      value == other.value
    end

    def validate(other)
      unless @value == other
        raise LiteralValidationError.new(
                expected_value: @value, received_value: other
              )
      end

      true
    end
  end
end
