require_relative './scalar.rb'

module TSJSON
  class StringType < ScalarType
    def initialize
      super('String')
    end

    def validate(value)
      super(value) unless value.is_a?(::String)

      true
    end
  end

  TSJSONString = StringType.new
end
