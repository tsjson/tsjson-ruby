module TSJSON
  class LiteralUnionValidationError < ValidationError
    def initialize(expected_values:, received_value:)
      super
    end

    def to_human_json
      {
        message:
          "Expected values: `#{
            @details[:expected_values].join(', ')
          }`. Received value `#{@details[:received_value]}` of type `#{
            @details[:received_type].class
          }`"
      }
    end
  end
end
