module TSJSON
  class ListValidationError < ValidationError
    def initialize(errors:)
      super
    end

    def to_json
      {
        code: self.class.name.split('::').last,
        details: {
          errors:
            @details[:errors].map do |e|
              { index: e[:index], error: e[:error].to_json }
            end
        }
      }
    end

    def to_human_json
      details =
        @details[:errors].map do |err|
          index = err[:index]
          err_human_json = err[:error].to_human_json

          {
            message: "#{index}: #{err_human_json[:message]}",
            details: err_human_json[:details]
          }.compact
        end

      return { message: 'One or more items are invalid', details: details }
    end
  end
end
