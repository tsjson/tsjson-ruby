module TSJSON
  class ValidationError < StandardError
    def initialize(**details)
      @details = details
      super(to_json.to_s)
    end

    def to_json
      { code: self.class.name.split('::').last, details: @details }
    end

    def to_human_json
      { message: self.class.name.split('::').last }
    end
  end
end
