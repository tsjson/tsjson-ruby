module TSJSON
  class ScalarValidationError < ValidationError
    def initialize(expected_type:, received_type:, received_value:)
      super
    end

    def to_human_json
      {
        message:
          "Expected type `#{@details[:expected_type]}`. Received value `#{
            @details[:received_value]
          }` of type `#{@details[:received_type]}`"
      }
    end
  end
end
