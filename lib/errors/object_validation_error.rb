module TSJSON
  class ObjectValidationError < ValidationError
    def initialize(errors:)
      super
    end

    def to_json
      {
        code: self.class.name.split('::').last,
        details: {
          errors:
            @details[:errors].map do |e|
              { field: e[:field], error: e[:error].to_json }
            end
        }
      }
    end

    def to_human_json
      required_fields = []
      unexpected_fields = []
      other_errors = []

      @details[:errors].each do |err|
        if (err[:error].is_a?(RequiredFieldError))
          required_fields.push(err[:field])
        elsif (err[:error].is_a?(UnexpectedFieldError))
          unexpected_fields.push(err[:field])
        else
          err_human_json = err[:error].to_human_json
          other_errors.push(
            {
              message: "#{err[:field]}: #{err_human_json[:message]}",
              details: err_human_json[:details]
            }.compact
          )
        end
      end

      details = []
      if required_fields.length > 0
        details.push(
          { message: "Required fields: #{required_fields.join(', ')}" }
        )
      end
      if unexpected_fields.length > 0
        details.push(
          { message: "Unexpected fields: #{unexpected_fields.join(', ')}" }
        )
      end
      details.concat(other_errors)

      { message: 'Invalid object', details: details }
    end
  end
end
