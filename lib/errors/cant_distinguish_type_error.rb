module TSJSON
  class CantDistinguishTypeError < ValidationError
    def initialize(discriminators:)
      super
    end

    def to_human_json
      {
        message: "Can't distinguish type. Need more discriminators",
        details:
          @details[:discriminators].map do |d|
            { message: "#{d[:field]}: #{d[:values].join(', ')}" }
          end
      }
    end
  end
end
