module TSJSON
  class LiteralValidationError < ValidationError
    def initialize(expected_value:, received_value:)
      super
    end

    def to_human_json
      {
        message:
          "Expected value `#{@details[:expected_value]}`. Received value `#{
            @details[:received_value]
          }`"
      }
    end
  end
end
