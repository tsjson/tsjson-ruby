module TSJSON
  class ScalarUnionValidationError < ValidationError
    def initialize(expected_types:, received_type:, received_value:)
      super
    end

    def to_human_json
      {
        message:
          "Expected types: `#{
            @details[:expected_types].join(', ')
          }`. Received value `#{@details[:received_value]}` of type `#{
            @details[:received_type]
          }`"
      }
    end
  end
end
