module TSJSON
  class UnexpectedValueError < ValidationError
    def initialize(field:, expected_values:, received:)
      super
    end

    def to_human_json
      {
        message:
          "Field '#{@details[:field]}' received unexpected value '#{
            @details[:received]
          }'. Expected values are: #{@details[:expected_values].join(', ')}"
      }
    end
  end
end
