require_relative '../lexer/lexer.rb'
require_relative '../source.rb'
require_relative '../ast/kind.rb'
require_relative '../lexer/token_kind.rb'

module TSJSON
  class Parser
    def initialize(source)
      source = Source.new(source) unless source.is_a?(Source)
      @lexer = Lexer.new(source)
    end

    def parse_document
      return(
        {
          kind: AST::Kind::Document,
          definitions:
            parse_optional_many(
              TokenKind::SOF,
              :parse_definition,
              TokenKind::EOF
            )
        }
      )
    end

    def parse_definition
      if (peek(TokenKind::NAME))
        case current_token.value
        when 'type'
          return parse_type_alias_definition
        when 'enum'
          return parse_enum_definition
        end
      end

      raise unexpected
    end

    def parse_type_alias_definition
      start = expect_token(TokenKind::NAME)

      name = parse_name
      parameters =
        parse_optional_many(
          TokenKind::CHEVRON_L,
          :parse_type_parameter,
          TokenKind::CHEVRON_R,
          TokenKind::COMMA
        )

      expect_token(TokenKind::EQUALS)
      definition = parse_operation

      expect_optional_token(TokenKind::SEMICOLON)

      return(
        {
          kind: AST::Kind::TypeAlias,
          name: name,
          parameters: parameters,
          definition: definition,
          loc: loc(start)
        }
      )
    end

    def parse_type_parameter
      start = current_token
      name = parse_name

      return { kind: AST::Kind::TypeParameter, name: name, loc: loc(start) }
    end

    def parse_name
      token = expect_token(TokenKind::NAME)
      return { kind: AST::Kind::Name, value: token.value, loc: loc(token) }
    end

    def parse_type_definition()
      start = current_token
      type = nil
      if (peek(TokenKind::NAME))
        type = parse_type_reference
      elsif (peek(TokenKind::BRACE_L))
        type = parse_type_literal
      elsif (peek(TokenKind::PAREN_L))
        type = parse_parenthesized_type
      elsif (peek(TokenKind::BRACKET_L))
        type = parse_tuple
      elsif (peek(TokenKind::STRING))
        type = parse_string_literal
      elsif (peek(TokenKind::INT))
        type = parse_int
      elsif (peek(TokenKind::FLOAT))
        type = parse_float
      end

      throw unexpected if (!type)

      loop do
        unless [TokenKind::BRACKET_L, TokenKind::DOT].include?(
                 current_token.kind
               )
          break
        end

        if (expect_optional_token(TokenKind::BRACKET_L))
          if (expect_optional_token(TokenKind::BRACKET_R))
            type = { kind: AST::Kind::ArrayType, type: type, loc: loc(start) }
          else
            index = parse_string_literal
            expect_token(TokenKind::BRACKET_R)
            type = {
              kind: AST::Kind::IndexAccess,
              target: type,
              index: index,
              loc: loc(start)
            }
          end
        elsif (expect_optional_token(TokenKind::DOT))
          property = parse_name
          type = {
            kind: AST::Kind::PropertyAccess,
            target: type,
            property: property,
            loc: loc(start)
          }
        end
      end

      if (expect_optional_token(TokenKind::BRACKET_L))
        expect_token(TokenKind::BRACKET_R)
        type = { kind: AST::Kind::ArrayType, type: type, loc: loc(start) }
      end

      return type
    end

    def parse_operation
      expect_optional_token(TokenKind::PIPE)
      nodes = [parse_type_definition]
      operations = []

      execute_operation =
        lambda do
          operation = operations.shift
          right = nodes.pop
          left = nodes.pop

          new_operation = nil
          if (operation == TokenKind::AMP)
            if (left[:kind] == AST::Kind::IntersectionType)
              new_operation = {
                kind: AST::Kind::IntersectionType,
                types: left[:types].concat([right])
              }
            else
              new_operation = {
                kind: AST::Kind::IntersectionType,
                types: [left, right]
              }
            end
          elsif (operation == TokenKind::PIPE)
            if (left[:kind] == AST::Kind::UnionType)
              new_operation = {
                kind: AST::Kind::UnionType,
                types: left[:types].concat([right])
              }
            else
              new_operation = {
                kind: AST::Kind::UnionType,
                types: [left, right]
              }
            end
          end
          nodes.push(new_operation)
        end

      operation_kind = nil
      loop do
        unless (
                 LexerUtils.is_operation_token(
                   (operation_kind = current_token.kind)
                 )
               )
          break
        end
        loop do
          unless (
                   operations.length > 0 &&
                     TokenKind.operation_precedence(operations[0]) >=
                       TokenKind.operation_precedence(operation_kind)
                 )
            break
          end
          execute_operation.call
        end

        operations.unshift(operation_kind)
        @lexer.advance
        nodes.push(parse_type_definition)
      end

      loop do
        break unless (operations.length > 0)
        execute_operation.call
      end

      return nodes[0]
    end

    def parse_string_literal()
      token = expect_token(TokenKind::STRING)
      return(
        { kind: AST::Kind::StringLiteral, value: token.value, loc: loc(token) }
      )
    end

    def parse_int()
      token = expect_token(TokenKind::INT)
      return { kind: AST::Kind::Int, value: token.value.to_i, loc: loc(token) }
    end

    def parse_float()
      token = expect_token(TokenKind::FLOAT)
      return(
        { kind: AST::Kind::Float, value: token.value.to_f, loc: loc(token) }
      )
    end

    def parse_type_reference
      start = current_token
      name = parse_name
      args =
        parse_optional_many(
          TokenKind::CHEVRON_L,
          :parse_operation,
          TokenKind::CHEVRON_R,
          TokenKind::COMMA
        )

      return(
        {
          kind: AST::Kind::TypeReference,
          name: name,
          args: args,
          loc: loc(start)
        }
      )
    end

    def parse_tuple
      start = current_token
      types =
        parse_many(
          TokenKind::BRACKET_L,
          :parse_type_definition,
          TokenKind::BRACKET_R,
          TokenKind::COMMA
        )
      return { kind: AST::Kind::Tuple, types: types, loc: loc(start) }
    end

    def parse_type_literal
      start = current_token
      properties =
        parse_optional_many(
          TokenKind::BRACE_L,
          :parse_property_signature,
          TokenKind::BRACE_R
        )
      return(
        {
          kind: AST::Kind::TypeLiteral,
          properties: properties,
          loc: loc(start)
        }
      )
    end

    def parse_property_signature()
      start = current_token
      name = parse_name
      optional = expect_optional_token(TokenKind::QUESTION_MARK).nil?.!
      expect_token(TokenKind::COLON)
      type = parse_operation
      expect_token(TokenKind::SEMICOLON)

      return(
        {
          kind: AST::Kind::PropertySignature,
          name: name,
          type: type,
          optional: optional,
          loc: loc(start)
        }
      )
    end

    def parse_parenthesized_type()
      start = expect_token(TokenKind::PAREN_L)
      type = parse_operation
      expect_token(TokenKind::PAREN_R)
      return { kind: AST::Kind::ParenthesizedType, type: type, loc: loc(start) }
    end

    def parse_enum_definition
      start = expect_token(TokenKind::NAME)
      name = parse_name
      members =
        parse_many(
          TokenKind::BRACE_L,
          :parse_enum_member,
          TokenKind::BRACE_R,
          TokenKind::COMMA
        )
      expect_optional_token(TokenKind::SEMICOLON)

      return(
        { kind: AST::Kind::Enum, name: name, members: members, loc: loc(start) }
      )
    end

    def parse_enum_member
      start = current_token
      name = parse_name

      value = nil
      value = parse_string_literal if (expect_optional_token(TokenKind::EQUALS))

      return(
        {
          kind: AST::Kind::EnumMember,
          name: name,
          value: value,
          loc: loc(start)
        }
      )
    end

    #######

    # Returns current token from lexer
    def current_token
      @lexer.token
    end

    # Helper function for creating an error when an unexpected lexed token is encountered.
    def unexpected(at_token = nil)
      token = at_token || current_token
      return(
        TSJSONSyntaxError.syntax_error(
          @lexer.source,
          token.start_pos,
          "Unexpected #{LexerUtils.get_token_desc(token)}."
        )
      )
    end

    # Returns a location object, used to identify the place in the source that created a given parsed object.
    def loc(start_token)
      end_token = @lexer.last_token
      return {} #Location.new(start_token, end_token, @lexer.source)
    end

    # Determines if the next token is of a given kind
    def peek(kind)
      current_token.kind == kind
    end

    # Returns a non-empty list of parse nodes, determined by the parseFn.
    # This list begins with a lex token of openKind and ends with a lex token of closeKind.
    # Advances the parser to the next lex token after the closing token.
    def parse_many(open_kind, parse_fn_symbol, close_kind, delimeter_kind = nil)
      expect_token(open_kind)
      nodes = []
      loop do
        nodes.push(self.send(parse_fn_symbol))
        expect_token(delimeter_kind) if (delimeter_kind && !peek(close_kind))
        break if expect_optional_token(close_kind)
      end
      return nodes
    end

    # Returns a list of parse nodes, determined by the parseFn.
    # It can be empty only if open token is missing otherwise it will always return non-empty list
    # that begins with a lex token of openKind and ends with a lex token of closeKind.
    # Advances the parser to the next lex token after the closing token.
    def parse_optional_many(
      open_kind,
      parse_fn_symbol,
      close_kind,
      delimeter_kind = nil
    )
      if (expect_optional_token(open_kind))
        nodes = []
        loop do
          break if expect_optional_token(close_kind)
          nodes.push(self.send(parse_fn_symbol))
          expect_token(delimeter_kind) if (delimeter_kind && !peek(close_kind))
        end
        return nodes
      end
      return []
    end

    # If the next token is of the given kind, return that token after advancing the lexer.
    # Otherwise, do not change the parser state and throw an error.
    def expect_token(kind)
      token = current_token
      if (token.kind == kind)
        @lexer.advance
        return token
      end

      raise TSJSONSyntaxError.syntax_error(
              @lexer.source,
              token.start_pos,
              "Expected #{LexerUtils.get_token_kind_desc(kind)}, found #{
                LexerUtils.get_token_desc(token)
              }."
            )
    end

    # If the next token is of the given kind, return that token after advancing the lexer.
    # Otherwise, do not change the parser state and return undefined.
    def expect_optional_token(kind)
      token = current_token
      if token.kind === kind
        @lexer.advance
        return token
      end
      return nil
    end
  end
end
