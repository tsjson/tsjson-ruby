module TSJSON
  class Source
    attr_accessor :body, :name, :locationOffset

    def initialize(
      body, name = 'TSJSON Source', locationOffset = { line: 1, column: 1 }
    )
      self.body = body
      self.name = name
      self.locationOffset = locationOffset
    end

    def self.print_location(location)
      return(
        print_source_location(
          location.source,
          Location.get_location(location.source, location.start)
        )
      )
    end

    def self.print_source_location(source, sourceLocation)
      firstLineColumnOffset = source.locationOffset[:column] - 1
      body = whitespace(firstLineColumnOffset) + source.body

      lineIndex = sourceLocation[:line] - 1
      lineOffset = source.locationOffset[:line] - 1
      lineNum = sourceLocation[:line] + lineOffset

      columnOffset = sourceLocation[:line] === 1 ? firstLineColumnOffset : 0
      columnNum = sourceLocation[:column] + columnOffset
      locationStr = "#{source.name}:#{lineNum}:#{columnNum}\n"

      lines = body.split("\n", -1)
      locationLine = lines[lineIndex]

      # Special case for minified documents
      if (locationLine.length > 120)
        subLineIndex = Math.floor(columnNum / 80)
        subLineColumnNum = columnNum % 80
        subLines = []

        i = 0
        loop do
          break unless i < locationLine.length
          subLines.push(locationLine.slice(i, 80))
          i += 80
        end

        return(
          locationStr +
            print_prefixed_lines(
              [["#{lineNum}", subLines[0]]].concat(
                subLines.slice(1, subLineIndex + 1).map do |subLine|
                  ['', subLine]
                end
              ).concat(
                [
                  [' ', whitespace(subLineColumnNum - 1) + '^'],
                  ['', subLines[subLineIndex + 1]]
                ]
              )
            )
        )
      end

      return(
        locationStr +
          print_prefixed_lines(
            [
              ["#{lineNum - 1}", lineIndex > 1 ? lines[lineIndex - 1] : nil],
              ["#{lineNum}", locationLine],
              ['', whitespace(columnNum - 1) + '^'],
              ["#{lineNum + 1}", lines[lineIndex + 1]]
            ]
          )
      )
    end

    def self.whitespace(len)
      return Array.new(len + 1, '').join(' ')
    end

    def self.leftPad(len, str)
      return whitespace(len - str.length) + str
    end

    def self.print_prefixed_lines(lines)
      existingLines =
        lines.filter do |line_arr|
          _, line = line_arr
          line != nil
        end

      padLen =
        existingLines.map do |prefix_arr|
          prefix = prefix_arr[0]
          prefix.length
        end.max

      return(
        existingLines.map do |line_arr|
          prefix, line = line_arr
          leftPad(padLen, prefix) + (line.empty? ? ' |' : ' | ' + line)
        end.join("\n")
      )
    end
  end
end
