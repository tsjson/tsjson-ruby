module TSJSON
  module TokenKind
    SOF = '<SOF>'
    EOF = '<EOF>'
    AMP = '&'
    PIPE = '|'
    PAREN_L = '('
    PAREN_R = ')'
    BRACKET_L = '['
    BRACKET_R = ']'
    BRACE_L = '{'
    BRACE_R = '}'
    CHEVRON_L = '<'
    CHEVRON_R = '>'
    COLON = ':'
    SEMICOLON = ';'
    EQUALS = '='
    COMMA = ','
    DOT = '.'
    QUESTION_MARK = '?'
    NAME = 'Name'
    INT = 'Int'
    FLOAT = 'Float'
    STRING = 'String'
    BLOCK_STRING = 'BlockString'
    COMMENT = 'Comment'

    def self.operation_precedence(kind)
      case (kind)
      when TokenKind::AMP
        return 2
      when TokenKind::PIPE
        return 1
      end
    end
  end
end
