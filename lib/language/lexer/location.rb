module TSJSON
  class Location
    def self.get_location(source, position)
      lineRegexp = /\r\n|[\n\r]/
      line = 1
      column = position + 1
      body = source.body
      new_lines_indexes = (0...body.length).find_all { |i| body[i, 1] == "\n" }

      new_lines_indexes.each do |index|
        break if index >= position

        line += 1
        column = position + 1 - (index + 1)
      end

      return { line: line, column: column }
    end
  end
end
