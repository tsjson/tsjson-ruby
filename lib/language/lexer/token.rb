module TSJSON
  class Token
    attr_accessor :kind,
                  :start_pos,
                  :end_pos,
                  :line,
                  :column,
                  :value,
                  :prev,
                  :next

    def initialize(kind, start_pos, end_pos, line, column, prev, value = nil)
      self.kind = kind
      self.start_pos = start_pos
      self.end_pos = end_pos
      self.line = line
      self.column = column
      self.value = value
      self.prev = prev
      self.next = nil
    end

    def toJSON
      {
        kind: self.kind,
        start: self.start_pos,
        end: self.end_pos,
        line: self.line,
        column: self.column,
        value: self.value
      }.compact
    end
  end
end
