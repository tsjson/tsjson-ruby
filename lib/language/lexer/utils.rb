module TSJSON
  class LexerUtils
    class << self
      def is_punctuator_token_kind?(kind)
        return(
          kind === TokenKind::PIPE || kind === TokenKind::AMP ||
            kind === TokenKind::COLON || kind === TokenKind::EQUALS ||
            kind === TokenKind::CHEVRON_L || kind === TokenKind::CHEVRON_R ||
            kind === TokenKind::PAREN_L || kind === TokenKind::PAREN_R ||
            kind === TokenKind::BRACKET_L || kind === TokenKind::BRACKET_R ||
            kind === TokenKind::BRACE_L || kind === TokenKind::BRACE_R
        )
      end

      def is_operation_token(kind)
        return kind == TokenKind::PIPE || kind == TokenKind::AMP
      end

      def get_token_desc(token)
        value = token.value
        return(
          get_token_kind_desc(token.kind) +
            (value != nil ? " \"#{value}\"" : '')
        )
      end

      def get_token_kind_desc(kind)
        return is_punctuator_token_kind?(kind) ? "\"#{kind}\"" : kind
      end
    end
  end
end
