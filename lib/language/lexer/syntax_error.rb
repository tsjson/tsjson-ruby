require_relative './location.rb'

module TSJSON
  class TSJSONSyntaxError < StandardError
    attr_accessor :message,
                  :nodes,
                  :source,
                  :positions,
                  :path,
                  :locations,
                  :originalError

    def initialize(
      message, nodes, source, positions, path = nil, originalError = nil
    )
      self.message = message

      _nodes =
        if nodes.is_a?(Array)
          nodes.length != 0 ? nodes : nil
        else
          nodes ? [nodes] : nil
        end

      # Compute locations in the source for the given nodes/positions.
      _source = source
      _source = _nodes[0].loc&.source if (!_source && _nodes)

      _positions = positions

      if (!_positions && _nodes)
        _positions =
          _nodes.reduce([]) do |list, node|
            list.push(node.loc.start) if (node.loc)
            return list
          end
      end
      _positions = undefined if (_positions && _positions.length === 0)

      if (positions && source)
        _locations = positions.map { |pos| Location.get_location(source, pos) }
      elsif (_nodes)
        _locations =
          _nodes.reduce([]) do |list, node|
            if (node.loc)
              list.push(
                Location.get_location(node.loc.source, node.loc.start_pos)
              )
            end
            return list
          end
      end

      self.source = _source
      self.positions = _positions
      self.locations = _locations
      self.nodes = _nodes
    end

    def self.print_error(error)
      output = error.message

      if (error.nodes)
        error.nodes.each do |node|
          output += "\n\n" + printLocation(node.loc) if (node.loc)
        end
      elsif (error.source && error.locations)
        error.locations.each do |location|
          output +=
            "\n\n" + Source.print_source_location(error.source, location)
        end
      end

      return output
    end

    def self.syntax_error(source, position, description)
      return new("Syntax Error: #{description}", nil, source, [position])
    end

    def toJSON
      return { message: self.message, locations: self.locations }
    end

    def to_s
      return TSJSONSyntaxError.print_error(self)
    end
  end
end
