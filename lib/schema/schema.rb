require_relative '../types/index.rb'
require_relative './schema_builder'

module TSJSON
  class Schema
    attr_reader :types_map

    BUILT_IN_SCALARS = {
      'string' => TSJSONString,
      'int' => TSJSONInt,
      'float' => TSJSONFloat,
      'number' => TSJSONFloat,
      'boolean' => TSJSONBoolean,
      'any' => TSJSONAny,
      'null' => TSJSONNull
    }

    def initialize
      @types_map = {}
    end

    def type(name)
      BUILT_IN_SCALARS[name] || @types_map[name]
    end

    def add_type(name, type)
      raise 'duplicate type' if type(name)

      @types_map[name] = type
    end

    def compile
      @types_map.values.each(&:compile)
    end

    def self.build(source)
      unless source.is_a?(Array) || source.is_a?(String)
        raise 'Schema.build expects string or array of strings'
      end
      builder = SchemaBuilder.new
      (source.is_a?(Array) ? source : [source]).each do |s|
        builder.parse_source(s)
      end
      builder.schema.compile
      return builder.schema
    end
  end
end
