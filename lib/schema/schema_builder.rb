require_relative '../language/parser/parser.rb'

module TSJSON
  class SchemaBuilder
    attr_reader :schema

    def initialize
      @schema = Schema.new
      @generics = {}
    end

    def add_generic(name, parameters_ast, type_ast)
      check_duplicate_type(name)

      parameters = parameters_ast.map { |p| { name: p[:name][:value] } }

      @generics[name] = { parameters: parameters, type_ast: type_ast }
    end

    def add_type(name, type_ast)
      check_duplicate_type(name)

      @schema.add_type(name, type_definition(type_ast))
    end

    def check_duplicate_type(name)
      if @schema.type(name) || @generics.key?(name)
        raise "duplicate type #{name}"
      end
    end

    def parse_source(source)
      parser = Parser.new(source)
      ast = parser.parse_document

      ast[:definitions].each { |node| definition(node) }
    end

    def definition(node)
      case node[:kind]
      when AST::Kind::TypeAlias
        name = node[:name][:value]
        parameters = node[:parameters]
        type_node = node[:definition]

        if parameters.length > 0
          add_generic(name, parameters, type_node)
        else
          add_type(name, type_node)
        end
        return
      when AST::Kind::Enum
        name = node[:name][:value]
        members =
          node[:members].reduce({}) do |acc, m|
            acc.merge(
              {
                m[:name][:value] =>
                  Literal.new(m.dig(:value, :value) || m.dig(:name, :value)) # use name as value if value is not defined
              }
            )
          end
        type = Enum.new(members)
        @schema.add_type(name, type)
        return
      end

      raise "Can't build definition. Kind: #{node[:kind]}"
    end

    def type_definition(node, scope = {})
      case node[:kind]
      when AST::Kind::TypeReference
        return reference(node, scope)
      when AST::Kind::ArrayType
        return List.new(type_definition(node[:type], scope))
      when AST::Kind::StringLiteral
        return Literal.new(node[:value])
      when AST::Kind::Int
        return Literal.new(node[:value])
      when AST::Kind::Float
        return Literal.new(node[:value])
      when AST::Kind::TypeLiteral
        return(
          ObjectType.new do
            node[:properties].reduce({}) do |map, f|
              map[f[:name][:value]] = {
                type: type_definition(f[:type], scope),
                optional: f[:optional]
              }
              map
            end
          end
        )
      when AST::Kind::IntersectionType
        return(
          Intersection.new(node[:types].map { |t| type_definition(t, scope) })
        )
      when AST::Kind::UnionType
        types = node[:types].map { |t| type_definition(t, scope) }
        t = types.first

        if t.is_a?(ObjectType) || t.is_a?(Union) || t.is_a?(Intersection)
          return Union.new(types)
        elsif t.is_a?(ScalarType)
          return ScalarUnion.new(types)
        elsif t.is_a?(Literal)
          return LiteralUnion.new(types)
        end
        raise "can't create union with type #{t.class.name}"
      when AST::Kind::ParenthesizedType
        return type_definition(node[:type], scope)
      when AST::Kind::PropertyAccess
        target = type_definition(node[:target], scope)
        return target.property(node[:property][:value])
      when AST::Kind::IndexAccess
        target = type_definition(node[:target], scope)
        return target.index(node[:index][:value])
      end

      raise "Can't build type definition. Kind: #{node[:kind]}"
    end

    def reference(node, scope)
      name = node[:name][:value]

      if type = scope[name] || @schema.type(name)
        return type
      elsif generic = @generics[name]
        parameters = generic[:parameters]
        args = node[:args]

        new_scope =
          generic[:parameters]
            .each_with_index
            .reduce(scope) do |memo, (param, idx)|
              name = param[:name]
              type_ast = args[idx]
              raise "argument #{name} was not provided" if (!type_ast)

              memo.merge({ name => type_definition(type_ast, scope) })
            end
        return type_definition(generic[:type_ast], new_scope)
      end
      raise "Type doesn't exist: #{name}" unless type
    end
  end
end
