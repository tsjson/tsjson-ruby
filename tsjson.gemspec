Gem::Specification.new do |s|
  s.name = 'tsjson'
  s.version = '1.0.0'
  s.date = '2021-01-02'
  s.authors = %w[vitramir]
  s.summary = 'ruby implementation for tsjson lang'
  s.files = Dir['lib/**/*.rb']
end
